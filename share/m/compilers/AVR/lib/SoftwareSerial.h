#ifndef SoftwareSerial_h
#define SoftwareSerial_h

#include <inttypes.h>
#include <Stream.h>

#ifndef _SS_MAX_RX_BUFF
#define _SS_MAX_RX_BUFF 64 // RX buffer size
#endif

#ifndef GCC_VERSION
#define GCC_VERSION (__GNUC__ * 10000 + __GNUC_MINOR__ * 100 + __GNUC_PATCHLEVEL__)
#endif

class SoftwareSerial : public Stream {
	private:
		uint8_t _receivePin;
		uint8_t _receiveBitMask;
		volatile uint8_t *_receivePortRegister;
		uint8_t _transmitBitMask;
		volatile uint8_t *_transmitPortRegister;
		volatile uint8_t *_pcint_maskreg;
		uint8_t _pcint_maskvalue;
		uint16_t _rx_delay_centering;
		uint16_t _rx_delay_intrabit;
		uint16_t _rx_delay_stopbit;
		uint16_t _tx_delay;
		uint16_t _buffer_overflow: 1;
		uint16_t _inverse_logic: 1;
		static uint8_t _receive_buffer[_SS_MAX_RX_BUFF];
		static volatile uint8_t _receive_buffer_tail;
		static volatile uint8_t _receive_buffer_head;
		static SoftwareSerial *active_object;
		inline void recv() __attribute__( ( __always_inline__ ) );
		uint8_t rx_pin_read();
		void setTX( uint8_t transmitPin );
		void setRX( uint8_t receivePin );
		inline void setRxIntMsk( bool enable ) __attribute__( ( __always_inline__ ) );
		static uint16_t subtract_cap( uint16_t num, uint16_t sub );
		static inline void tunedDelay( uint16_t delay );
	public:
		SoftwareSerial( uint8_t receivePin, uint8_t transmitPin, bool inverse_logic = false );  // 重载串口
		~SoftwareSerial();
		void begin( long speed );  // 设置端口速率
		bool listen();  // 监听端口
		void end();
		bool isListening();
		bool stopListening();
		bool overflow();
		int peek();
		virtual size_t write( uint8_t byte );
		virtual int read();
		virtual int available();
		virtual void flush();
		operator bool() { return true; }
		using Print::write;
		static inline void handle_interrupt() __attribute__( ( __always_inline__ ) );
};

#endif
