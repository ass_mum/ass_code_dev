#define _DEBUG 0
#define _DEBUG_PIN1 11
#define _DEBUG_PIN2 13

#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <Arduino.h>
#include <SoftwareSerial.h>
#include <util/delay_basic.h>

SoftwareSerial *SoftwareSerial::active_object = 0;
uint8_t SoftwareSerial::_receive_buffer[_SS_MAX_RX_BUFF];
volatile uint8_t SoftwareSerial::_receive_buffer_tail = 0;
volatile uint8_t SoftwareSerial::_receive_buffer_head = 0;

inline void DebugPulse( uint8_t pin, uint8_t count ) {
	#if _DEBUG
	volatile uint8_t *pport = portOutputRegister( digitalPinToPort( pin ) );
	uint8_t val = *pport;
	while( count-- ) {
		*pport = val | digitalPinToBitMask( pin );
		*pport = val;
	}
	#endif
}

//! @brief
//! 重载串口端口io
//! @param receivePin uint8_t  RX端口io
//! @param transmitPin uint8_t  TX端口io
//! @param inverse_logic bool  逆向逻辑
//!
SoftwareSerial::SoftwareSerial( uint8_t receivePin, uint8_t transmitPin, bool inverse_logic ) :
	_rx_delay_centering( 0 ), _rx_delay_intrabit( 0 ), _rx_delay_stopbit( 0 ),
	_tx_delay( 0 ), _buffer_overflow( false ), _inverse_logic( inverse_logic ) {
	setTX( transmitPin );
	setRX( receivePin );
}

SoftwareSerial::~SoftwareSerial() {
	end();
}

inline void SoftwareSerial::tunedDelay( uint16_t delay ) {
	_delay_loop_2( delay );
}

//! @brief
//! 监听对象端口
//! @return bool
//! 若替换了另一个对象 则返回TRUE, 否则返回FALSE
//!
bool SoftwareSerial::listen() {
	if( !_rx_delay_stopbit ) {
		return false;
	}
	if( active_object != this ) {
		if( active_object ) {
			active_object->stopListening();
		}
		_buffer_overflow = false;
		_receive_buffer_head = _receive_buffer_tail = 0;
		active_object = this;
		setRxIntMsk( true );
		return true;
	}
	return false;
}

bool SoftwareSerial::isListening() {
	return this == active_object;
}

bool SoftwareSerial::stopListening() {
	if( active_object == this ) {
		setRxIntMsk( false );
		active_object = NULL;
		return true;
	}
	return false;
}

bool SoftwareSerial::overflow() {
	bool ret = _buffer_overflow;
	if( ret ) {
		_buffer_overflow = false;
	}
	return ret;
}

void SoftwareSerial::recv() {
	#if GCC_VERSION < 40302
	asm volatile(
		"push r18 \n\t"
		"push r19 \n\t"
		"push r20 \n\t"
		"push r21 \n\t"
		"push r22 \n\t"
		"push r23 \n\t"
		"push r26 \n\t"
		"push r27 \n\t"
		:: );
	#endif
	uint8_t d = 0;
	if( _inverse_logic ? rx_pin_read() : !rx_pin_read() ) {
		setRxIntMsk( false );
		tunedDelay( _rx_delay_centering );
		DebugPulse( _DEBUG_PIN2, 1 );
		for( uint8_t i = 8; i > 0; --i ) {
			tunedDelay( _rx_delay_intrabit );
			d >>= 1;
			DebugPulse( _DEBUG_PIN2, 1 );
			if( rx_pin_read() ) {
				d |= 0x80;
			}
		}
		if( _inverse_logic ) {
			d = ~d;
		}
		uint8_t next = ( _receive_buffer_tail + 1 ) % _SS_MAX_RX_BUFF;
		if( next != _receive_buffer_head ) {
			_receive_buffer[_receive_buffer_tail] = d;
			_receive_buffer_tail = next;
		} else {
			DebugPulse( _DEBUG_PIN1, 1 );
			_buffer_overflow = true;
		}
		tunedDelay( _rx_delay_stopbit );
		DebugPulse( _DEBUG_PIN1, 1 );
		setRxIntMsk( true );
	}
	#if GCC_VERSION < 40302
	asm volatile(
		"pop r27 \n\t"
		"pop r26 \n\t"
		"pop r23 \n\t"
		"pop r22 \n\t"
		"pop r21 \n\t"
		"pop r20 \n\t"
		"pop r19 \n\t"
		"pop r18 \n\t"
		:: );
	#endif
}

uint8_t SoftwareSerial::rx_pin_read() {
	return *_receivePortRegister & _receiveBitMask;
}

inline void SoftwareSerial::handle_interrupt() {
	if( active_object ) {
		active_object->recv();
	}
}

#if defined(PCINT0_vect)
ISR( PCINT0_vect ) {
	SoftwareSerial::handle_interrupt();
}
#endif

#if defined(PCINT1_vect)
ISR( PCINT1_vect, ISR_ALIASOF( PCINT0_vect ) );
#endif

#if defined(PCINT2_vect)
ISR( PCINT2_vect, ISR_ALIASOF( PCINT0_vect ) );
#endif

#if defined(PCINT3_vect)
ISR( PCINT3_vect, ISR_ALIASOF( PCINT0_vect ) );
#endif

void SoftwareSerial::setTX( uint8_t tx ) {
	digitalWrite( tx, _inverse_logic ? LOW : HIGH );
	pinMode( tx, OUTPUT );
	_transmitBitMask = digitalPinToBitMask( tx );
	uint8_t port = digitalPinToPort( tx );
	_transmitPortRegister = portOutputRegister( port );
}

void SoftwareSerial::setRX( uint8_t rx ) {
	pinMode( rx, INPUT );
	if( !_inverse_logic ) {
		digitalWrite( rx, HIGH );
	}
	_receivePin = rx;
	_receiveBitMask = digitalPinToBitMask( rx );
	uint8_t port = digitalPinToPort( rx );
	_receivePortRegister = portInputRegister( port );
}

uint16_t SoftwareSerial::subtract_cap( uint16_t num, uint16_t sub ) {
	if( num > sub ) {
		return num - sub;
	} else {
		return 1;
	}
}

void SoftwareSerial::begin( long speed ) {
	_rx_delay_centering = _rx_delay_intrabit = _rx_delay_stopbit = _tx_delay = 0;
	uint16_t bit_delay = ( F_CPU / speed ) / 4;
	_tx_delay = subtract_cap( bit_delay, 15 / 4 );
	if( digitalPinToPCICR( ( int8_t )_receivePin ) ) {
		#if GCC_VERSION > 40800
		_rx_delay_centering = subtract_cap( bit_delay / 2, ( 4 + 4 + 75 + 17 - 23 ) / 4 );
		_rx_delay_intrabit = subtract_cap( bit_delay, 23 / 4 );
		_rx_delay_stopbit = subtract_cap( bit_delay * 3 / 4, ( 37 + 11 ) / 4 );
		#else
		_rx_delay_centering = subtract_cap( bit_delay / 2, ( 4 + 4 + 97 + 29 - 11 ) / 4 );
		_rx_delay_intrabit = subtract_cap( bit_delay, 11 / 4 );
		_rx_delay_stopbit = subtract_cap( bit_delay * 3 / 4, ( 44 + 17 ) / 4 );
		#endif
		*digitalPinToPCICR( ( int8_t )_receivePin ) |= _BV( digitalPinToPCICRbit( _receivePin ) );
		_pcint_maskreg = digitalPinToPCMSK( _receivePin );
		_pcint_maskvalue = _BV( digitalPinToPCMSKbit( _receivePin ) );
		tunedDelay( _tx_delay );
	}
	#if _DEBUG
	pinMode( _DEBUG_PIN1, OUTPUT );
	pinMode( _DEBUG_PIN2, OUTPUT );
	#endif
	listen();
}

void SoftwareSerial::setRxIntMsk( bool enable ) {
	if( enable ) {
		*_pcint_maskreg |= _pcint_maskvalue;
	} else {
		*_pcint_maskreg &= ~_pcint_maskvalue;
	}
}

void SoftwareSerial::end() {
	stopListening();
}

int SoftwareSerial::read() {
	if( !isListening() ) {
		return -1;
	}
	if( _receive_buffer_head == _receive_buffer_tail ) {
		return -1;
	}
	uint8_t d = _receive_buffer[_receive_buffer_head];
	_receive_buffer_head = ( _receive_buffer_head + 1 ) % _SS_MAX_RX_BUFF;
	return d;
}

int SoftwareSerial::available() {
	if( !isListening() ) {
		return 0;
	}
	return ( _receive_buffer_tail + _SS_MAX_RX_BUFF - _receive_buffer_head ) % _SS_MAX_RX_BUFF;
}

size_t SoftwareSerial::write( uint8_t b ) {
	if( _tx_delay == 0 ) {
		setWriteError();
		return 0;
	}
	volatile uint8_t *reg = _transmitPortRegister;
	uint8_t reg_mask = _transmitBitMask;
	uint8_t inv_mask = ~_transmitBitMask;
	uint8_t oldSREG = SREG;
	bool inv = _inverse_logic;
	uint16_t delay = _tx_delay;
	if( inv ) {
		b = ~b;
	}
	cli();
	if( inv ) {
		*reg |= reg_mask;
	} else {
		*reg &= inv_mask;
	}
	tunedDelay( delay );
	for( uint8_t i = 8; i > 0; --i ) {
		if( b & 1 )
		{ *reg |= reg_mask; }
		else
		{ *reg &= inv_mask; }
		tunedDelay( delay );
		b >>= 1;
	}
	if( inv ) {
		*reg &= inv_mask;
	} else {
		*reg |= reg_mask;
	}
	SREG = oldSREG;
	tunedDelay( _tx_delay );
	return 1;
}

void SoftwareSerial::flush() {
}

int SoftwareSerial::peek() {
	if( !isListening() ) {
		return -1;
	}
	if( _receive_buffer_head == _receive_buffer_tail ) {
		return -1;
	}
	return _receive_buffer[_receive_buffer_head];
}
