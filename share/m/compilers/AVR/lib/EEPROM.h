#ifndef EEPROM_h
#define EEPROM_h

#include <inttypes.h>
#include <avr/eeprom.h>
#include <avr/io.h>

struct EERef {
	EERef( const int index ) : index( index ) {}
	uint8_t operator*() const { return eeprom_read_byte( ( uint8_t* ) index ); }
	operator uint8_t() const { return **this; }
	EERef &operator=( const EERef &ref ) { return *this = *ref; }
	EERef &operator=( uint8_t in ) { return eeprom_write_byte( ( uint8_t* ) index, in ), *this; }
	EERef &operator +=( uint8_t in ) { return *this = **this + in; }
	EERef &operator -=( uint8_t in ) { return *this = **this - in; }
	EERef &operator *=( uint8_t in ) { return *this = **this * in; }
	EERef &operator /=( uint8_t in ) { return *this = **this / in; }
	EERef &operator ^=( uint8_t in ) { return *this = **this ^ in; }
	EERef &operator %=( uint8_t in ) { return *this = **this % in; }
	EERef &operator &=( uint8_t in ) { return *this = **this & in; }
	EERef &operator |=( uint8_t in ) { return *this = **this | in; }
	EERef &operator <<=( uint8_t in ) { return *this = **this << in; }
	EERef &operator >>=( uint8_t in ) { return *this = **this >> in; }
	EERef &update( uint8_t in ) { return in != *this ? *this = in : *this; }
	EERef& operator++() { return *this += 1; }
	EERef& operator--() { return *this -= 1; }
	uint8_t operator++ ( int ) {
		uint8_t ret = **this;
		return ++( *this ), ret;
	}
	uint8_t operator-- ( int ) {
		uint8_t ret = **this;
		return --( *this ), ret;
	}
	int index;
};

struct EEPtr {
	EEPtr( const int index ) : index( index ) {}
	operator int() const { return index; }
	EEPtr &operator=( int in ) { return index = in, *this; }
	bool operator!=( const EEPtr &ptr ) { return index != ptr.index; }
	EERef operator*() { return index; }
	EEPtr& operator++() { return ++index, *this; }
	EEPtr& operator--() { return --index, *this; }
	EEPtr operator++ ( int ) { return index++; }
	EEPtr operator-- ( int ) { return index--; }

	int index;
};

struct EEPROMClass {
	EERef operator[]( const int idx ) { return idx; }
	uint8_t read( int idx ) { return EERef( idx ); }  // 读取EEPROM内容
	void write( int idx, uint8_t val ) { ( EERef( idx ) ) = val; }  // 写入EEPROM内容
	void update( int idx, uint8_t val ) { EERef( idx ).update( val ); }  // 更新EEPROM内容
	EEPtr begin() { return 0x00; }
	EEPtr end() { return length(); }
	uint16_t length() { return E2END + 1; }
	template< typename T > T &get( int idx, T &t ) {
		EEPtr e = idx;
		uint8_t *ptr = ( uint8_t* ) &t;
		for( int count = sizeof( T ) ; count ; --count, ++e ) {
			*ptr++ = *e;
		}
		return t;
	}
	template< typename T > const T &put( int idx, const T &t ) {
		EEPtr e = idx;
		const uint8_t *ptr = ( const uint8_t* ) &t;
		for( int count = sizeof( T ) ; count ; --count, ++e ) {
			( *e ).update( *ptr++ );
		}
		return t;
	}
};

static EEPROMClass EEPROM;

#endif
