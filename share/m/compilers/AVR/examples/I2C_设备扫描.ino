#include <Wire.h>

void setup() {
	Wire.begin();
	Serial.begin( 9600 );  // 开启串口总线
	while( !Serial );  // 等待串口被打开
	Serial.println( "\nI2C 扫描已准备" );
}

void loop() {
	int nDevices = 0;
	Serial.println( "开始扫描..." );
	for( byte address = 1; address < 127; ++address ) {  // 查看设备是否对该地址作出应答
		Wire.beginTransmission( address );
		byte error = Wire.endTransmission();
		if( error == 0 ) {
			Serial.print( "I2C设备的地址位于 0x" );
			if( address < 16 ) {
				Serial.print( "0" );
			}
			Serial.print( address, HEX );
			Serial.println( "  !" );
			++nDevices;
		} else if( error == 4 ) {
			Serial.print( "未知错误的地址为 0x" );
			if( address < 16 ) {
				Serial.print( "0" );
			}
			Serial.println( address, HEX );
		}
	}
	if( nDevices == 0 ) {
		Serial.println( "没有发现I2C设备\n" );
	} else {
		Serial.println( "完成\n" );
	}
	delay( 5000 ); // 等待5秒进行下一次扫描
}
