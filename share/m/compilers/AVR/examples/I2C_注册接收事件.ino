#include <Wire.h>

void setup() {
  Wire.begin(8);  // 开启I2C总线 #8 地址
  Wire.onReceive(receiveEvent); // 注册事件
	Serial.begin( 9600 );  // 开启串口总线
}

void loop() {
  delay(100);
}

void receiveEvent(int howMany) {  // 当从主服务器接收到数据时执行
  while (1 < Wire.available()) {  // 循环遍历所有 但最后一个除外
    char c = Wire.read();  // 接收字符字节
    Serial.print(c);  // 打印字符
  }
  int x = Wire.read();  // 以整数形式接收字节
  Serial.println(x);  // 打印整数
}
