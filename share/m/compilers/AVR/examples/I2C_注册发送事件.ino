#include <Wire.h>

void setup() {
  Wire.begin(8);  // 开启I2C总线 #8 地址
  Wire.onRequest(requestEvent); // 注册事件
}

void loop() {
  delay(100);
}

void requestEvent() {  // 发送消息
  Wire.write("hello "); // 6字节的消息
}
