#include <Wire.h>

void setup() {
	Wire.begin();  // 开启I2C总线
}

byte x = 0;

void loop() {
	Wire.beginTransmission( 8 );  // 传输设备 #8 地址
	Wire.write( "x is " );  // 发送5个字节
	Wire.write( x );  // 发送一个字节
	Wire.endTransmission();  // 停止传输
	x++;
	delay( 500 );
}
