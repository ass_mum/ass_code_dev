/* 接线对照表
               MFRC522      Arduino       Arduino   Arduino    Arduino          Arduino
               默认脚位     Uno/101       Mega      Nano v3    Leonardo/Micro   Pro Micro
   -----------------------------------------------------------------------------------------
   RST/Reset   RST          9             5         D9         RESET/ICSP-5     RST
   SPI SS      SDA(SS)      10            53        D10        10               10
   SPI MOSI    MOSI         11 / ICSP-4   51        D11        ICSP-4           16
   SPI MISO    MISO         12 / ICSP-1   50        D12        ICSP-1           14
   SPI SCK     SCK          13 / ICSP-3   52        D13        ICSP-3           15
*/

#include <SPI.h>
#include <MFRC522.h>

#define RST_PIN   9  // 可重新配置IO口
#define SS_PIN   10  // 可重新配置IO口

MFRC522 mfrc522( SS_PIN, RST_PIN );  // 创建MFRC522实例

void setup() {
	Serial.begin( 9600 );  // 打开本地串口
	while( !Serial );  // 等待串口连接
	SPI.begin();  // 打开SPI总线
	mfrc522.PCD_Init();  // 打开MFRC522
	delay( 4 );  // 可选的延迟防止设备未就绪
	mfrc522.PCD_DumpVersionToSerial();	// 显示MFRC522读卡器详细信息
	Serial.println( F( "MFRC522读卡器已就绪..." ) );
}

void loop() {
	if( mfrc522.PICC_IsNewCardPresent() ) {	 // 如果传感器/读取器上没有新卡，则退出循环
		if( mfrc522.PICC_ReadCardSerial() ) {	 // 选中卡片
			// 显示关于卡的调试信息 PICC_HaltA()被自动调用
			mfrc522.PICC_DumpToSerial( &( mfrc522.uid ) );
		}
	}
}
