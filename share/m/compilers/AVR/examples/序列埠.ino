#include <SoftwareSerial.h>

// 打开串口通讯指定io口
SoftwareSerial mySerial( 10, 11 ); // RX TX

void setup() {
	Serial.begin( 9600 );  // 打开本地串口
	while( !Serial ); // 等待串口连接
	Serial.println( "Goodnight moon!" );
	// 设置软件串口的数据速率
	mySerial.begin( 4800 );
	mySerial.println( "Hello, world?" );
}

void loop() {
	if( 1 ) {
		if( mySerial.available() ) {  // 等待本地数据
			Serial.write( mySerial.read() );  // 发送外部数据
		}
		if( Serial.available() ) {  // 等待外部串口数据
			mySerial.write( Serial.read() );  // 发送本地数据
		}
	} else {  // 监听端口演示
		mySerial.listen();
		while( mySerial.available() > 0 ) {
			char inByte = mySerial.read();
			Serial.write( inByte );
		}
	}
}

