#include <Wire.h>

void setup() {
	Wire.begin();  // 开启I2C总线
	Serial.begin( 9600 );  // 开启串口总线
}

void loop() {
	Wire.requestFrom( 8, 6 );  // 从设备请求6字节 #8 地址
	while( Wire.available() ) {  // 检查发送请求长度
		char c = Wire.read();  // 接收一个字节作为字符
		Serial.print( c );  // 打印字符
	}
	delay( 500 );
}
