format PE GUI 4.0 ;声明这是个窗口程序

include 'win32a.inc' ;载入窗口相关的头文件
	invoke	信息框,0,p_内容,p_标题,0 ;展开MessageBoxA的汇编码
	                                          ;注意invoke是伪指令用以API的使用
	cmp	eax,IDYES ;判断用户是否按了确认按钮
	jne	退出 ;是就跳到退出程序
退出:invoke	退出进程,0 ;结束进程并退出

p_内容 db '大家好我是墨馨 来我家玩呗',0
p_标题 db '嗨',0  ;申明两个字符串变量

data import ;创建函数输入表

 library kernel32,'KERNEL32.DLL',\
	 user32,'USER32.DLL' ;导入这两个库


 import kernel32,\
	退出进程,'ExitProcess' ;导入ExitProcess声明

 import user32,\
	信息框,'MessageBoxA' ;导入MessageBoxA声明

end data;函数输入表结束符
cseg SEGMENT PARA PUBLIC 'CODE'
start PROC FAR
ASSUME CS:CSEG,DS:DSEG,SS:STACK,ES:NOTHING
mov ax,dseg
mov ds,ax
mov dx,OFFSET greet
mov ah,09H
int 21H
mov ah,4CH
int 21H
start ENDP
cseg ENDS
END start