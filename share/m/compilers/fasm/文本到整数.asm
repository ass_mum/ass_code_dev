
format binary
use32
include 'MACRO\PROC32.INC'
pop ebp
proc   atodw   Stri
; ----------------------------------------
; Convert decimal string into dword value
; return value in eax
; ----------------------------------------

push esi
push edi

xor eax, eax
mov esi, [Stri]
mov esi,[esi]

xor ecx, ecx
xor edx, edx
mov al, [esi]
inc esi
cmp al, 2D
jne proceed
mov al, byte  [esi]
not edx
inc esi
jmp proceed

@@:
sub al, 30h
lea ecx, dword  [ecx+4*ecx]
lea ecx, dword  [eax+2*ecx]
mov al, byte  [esi]
inc esi

proceed:
or al, al
jne @B
lea eax, dword  [edx+ecx]
xor eax, edx

pop edi
pop esi

ret

endp

call atodw
