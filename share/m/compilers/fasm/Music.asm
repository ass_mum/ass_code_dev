;******************************************************
;* 文件名:Music.asm
;* 创建日期:2001.7.5
;* 作者:陈文尧
;* 功能:利用内存驻留在喇叭里播放曲谱
;******************************************************
include system.inc

;******************************************************
;* 以下代码由汇编专家产生,不要随便修改
;******************************************************
.CODE
	ifdef __COM__
		org	100h
	endif
@@Start:
;------------------------------------------------------
; 初始化数据段
;------------------------------------------------------
	InitDS	cs
;------------------------------------------------------
; 调用主函数
;------------------------------------------------------
	call	main
;------------------------------------------------------
; 正常退出,并驻留内存
;------------------------------------------------------
	TSR2	376d ;本程序的.com文件长376Byte

;++++++++++++++++++++++++++++++++++++++++++++++++++++++
;+ 从这里开始到@@EndTSR,你可以加入需要驻留的代码和数据
;++++++++++++++++++++++++++++++++++++++++++++++++++++++
new1ch proc near
       dec        byte ptr cs:Count
       jg		@Return
       ;
       pusha
       push		ds
       push		cs
       pop		ds
       ;
       mov		si,Pointer
       mov		bx,[si]
       mov		Count,bl
       add		Pointer,02h    
       cmp        Pointer,MUSIC_END
       jg		@First
       and		Count,3fh
       shr		bx,06h
       LibCall	Sound,bx
@End:
       pop		ds
       popa
@Return: 
       jmp		dword ptr cs:[old1ch]
@First:
       mov		Pointer,offset MUSIC
       mov		Count,24h
       LibCall	NoSound
       jmp		@End
new1ch endp

MUSIC     dw  196*64+2*MULT,262*64+3*MULT,262*64+1*MULT,262*64+2*MULT
          dw  330*64+2*MULT,294*64+3*MULT,262*64+1*MULT,294*64+2*MULT
          dw  330*64+1*MULT,294*64+1*MULT,262*64+4*MULT,330*64+2*MULT
          dw  394*64+2*MULT,440*64+4*MULT,440*64+2*MULT,394*64+3*MULT
          dw  330*64+1*MULT,330*64+1*MULT,262*64+1*MULT,294*64+3*MULT
          dw  262*64+1*MULT,294*64+2*MULT,330*64+1*MULT,294*64+1*MULT
          dw  262*64+3*MULT,230*64+1*MULT,230*64+2*MULT,196*64+2*MULT
          dw  262*64+7*MULT,440*64+2*MULT,394*64+3*MULT,330*64+1*MULT
          dw  330*64+1*MULT,262*64+1*MULT,294*64+3*MULT,262*64+1*MULT
          dw  294*64+2*MULT,440*64+2*MULT,394*64+3*MULT,330*64+1*MULT
          dw  330*64+2*MULT,394*64+2*MULT,440*64+7*MULT,523*64+2*MULT
          dw  394*64+3*MULT,330*64+1*MULT,330*64+1*MULT,262*64+1*MULT
          dw  294*64+3*MULT,262*64+1*MULT,294*64+2*MULT,330*64+1*MULT
          dw  294*64+1*MULT,262*64+3*MULT,230*64+1*MULT,230*64+2*MULT
          dw  196*64+2*MULT,262*64+7*MULT,440*64+2*MULT,394*64+3*MULT
          dw  330*64+1*MULT,330*64+1*MULT,262*64+1*MULT,294*64+3*MULT
          dw  262*64+1*MULT,294*64+2*MULT,440*64+2*MULT,394*64+3*MULT
          dw  330*64+1*MULT,330*64+2*MULT,394*64+2*MULT,440*64+7*MULT
          dw  523*64+2*MULT,394*64+3*MULT,330*64+1*MULT,330*64+1*MULT
          dw  262*64+1*MULT,294*64+3*MULT,262*64+1*MULT,294*64+2*MULT
          dw  330*64+1*MULT,294*64+1*MULT,262*64+3*MULT,230*64+1*MULT
          dw  230*64+2*MULT,196*64+2*MULT,262*64+7*MULT
MUSIC_END =   offset $
MULT      =   08h

Count		db    00h
Pointer	dw    MUSIC_END
old1ch	dd	?

@@EndTSR:
;++++++++++++++++++++++++++++++++++++++++++++++++++++++
;+ 主函数,加入驻留前需要做的代码
;++++++++++++++++++++++++++++++++++++++++++++++++++++++
main	proc
	LibCall		GetVect,1ch
	mov			word ptr old1ch,ax
	mov			word ptr old1ch+02h,dx
	LibCall		SetVect,1ch,<offset new1ch>
	;
	ret
main	endp

;******************************************************
;* 标志程序结束并指定程序入口
;******************************************************
	end	@@Start