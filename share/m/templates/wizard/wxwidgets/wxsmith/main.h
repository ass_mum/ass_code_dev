//! @brief
//! @file [FILENAME_PREFIX]main.h
//! @details
//! @author [AUTHOR_NAME]
//! @date [NOW]
//!

#ifndef [PROJECT_HDR]MAIN_H
#define [PROJECT_HDR]MAIN_H

[IF WXFRAME]
//(*Headers( [CLASS_PREFIX]Frame )
//*)

class [CLASS_PREFIX]Frame: public wxFrame {
	public:
		[CLASS_PREFIX]Frame( wxWindow* parent, wxWindowID id = -1 );
		virtual ~[CLASS_PREFIX]Frame();
	private:
		//(*Handlers( [CLASS_PREFIX]Frame )
		void OnQuit( wxCommandEvent& event );
		void OnAbout( wxCommandEvent& event );
		//*)
		//(*Identifiers( [CLASS_PREFIX]Frame )
		//*)
		//(*Declarations( [CLASS_PREFIX]Frame )
		//*)
	private:
		static const wxEventTableEntry sm_eventTableEntries[];
	protected:
		virtual const wxEventTable* GetEventTable() const;
		virtual wxEventHashTable&  GetEventHashTable() const;
	protected:
		static const wxEventTable  sm_eventTable;
		static wxEventHashTable  sm_eventHashTable;
};
[ENDIF WXFRAME]
[IF WXDIALOG]
//(*Headers( [CLASS_PREFIX]Dialog )
//*)

class [CLASS_PREFIX]Dialog: public wxDialog {
	public:
		[CLASS_PREFIX]Dialog( wxWindow* parent, wxWindowID id = -1 );
		virtual ~[CLASS_PREFIX]Dialog();
	private:
		//(*Handlers( [CLASS_PREFIX]Dialog )
		void OnQuit( wxCommandEvent& event );
		void OnAbout( wxCommandEvent& event );
		//*)
		//(*Identifiers( [CLASS_PREFIX]Dialog )
		//*)
		//(*Declarations( [CLASS_PREFIX]Dialog )
		//*)
	private:
		static const wxEventTableEntry sm_eventTableEntries[];
	protected:
		virtual const wxEventTable* GetEventTable() const;
		virtual wxEventHashTable& GetEventHashTable() const;
	protected:
		static const wxEventTable sm_eventTable;
		static wxEventHashTable sm_eventHashTable;
};
[ENDIF WXDIALOG]

#endif // [PROJECT_HDR]MAIN_H
