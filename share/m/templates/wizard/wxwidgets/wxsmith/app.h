#ifndef [PROJECT_HDR]APP_H
#define [PROJECT_HDR]APP_H

//! @file [FILENAME_PREFIX]app.h
//! @brief
//! @details
//! @author [AUTHOR_NAME]
//! @date [NOW]
//!

#include <wx/app.h>


class [CLASS_PREFIX]App : public wxApp {
  public:
    virtual bool OnInit();  // 初始化图形环境
};

extern [CLASS_PREFIX]App &wxGetApp();

#endif
