// ***************************************************************
//   时间: [NOW]
//   用户: [AUTHOR_NAME] ([AUTHOR_WWW])
// ***************************************************************

[PCH_INCLUDE]
#include "[FILENAME_PREFIX]Main.h"
#include <wx/dialog_msg.h>

//(*InternalHeaders( [CLASS_PREFIX][IF WXFRAME]Frame[ENDIF WXFRAME][IF WXDIALOG]Dialog[ENDIF WXDIALOG] )
//*)

[IF WXFRAME]
//(*IdInit( [CLASS_PREFIX]Frame )
//*)

const wxEventTable [CLASS_PREFIX]Frame::sm_eventTable = {
	&wxFrame::sm_eventTable, &[CLASS_PREFIX]Frame::sm_eventTableEntries[0]
};

const wxEventTable *[CLASS_PREFIX]Frame::GetEventTable() const {
	return &[CLASS_PREFIX]Frame::sm_eventTable;
}

wxEventHashTable [CLASS_PREFIX]Frame::sm_eventHashTable( [CLASS_PREFIX]Frame::sm_eventTable );

wxEventHashTable &[CLASS_PREFIX]Frame::GetEventHashTable() const {
	return [CLASS_PREFIX]Frame::sm_eventHashTable;
}

const wxEventTableEntry [CLASS_PREFIX]Frame::sm_eventTableEntries[] = {  // 事件列表
	//(*EventTable( [CLASS_PREFIX]Frame )
	//*)
	wxEventTableEntry( wxEVT_NULL, 0, 0, 0, 0 )
};

[CLASS_PREFIX]Frame::[CLASS_PREFIX]Frame( wxWindow* parent, wxWindowID id ) {  // 创建图形界面
	//(*Initialize( [CLASS_PREFIX]Frame )
	//*)
}

[CLASS_PREFIX]Frame::~[CLASS_PREFIX]Frame() {
	//(*Destroy( [CLASS_PREFIX]Frame )
	//*)
}

void [CLASS_PREFIX]Frame::OnQuit( wxCommandEvent& event ) {  // 退出程序触发此函数
	Close();
}

void [CLASS_PREFIX]Frame::OnAbout( wxCommandEvent& event ) {  // 单击按钮触发此函数
	wxString msg(L"哈喽 欢迎使用图形库");
	sms( msg, L"欢迎..." );
}

[ENDIF WXFRAME]
[IF WXDIALOG]
//(*IdInit( [CLASS_PREFIX]Dialog )
//*)

const wxEventTable [CLASS_PREFIX]Dialog::sm_eventTable = {
	&wxDialog::sm_eventTable, &[CLASS_PREFIX]Dialog::sm_eventTableEntries[0]
};

const wxEventTable *[CLASS_PREFIX]Dialog::GetEventTable() const {
	return &[CLASS_PREFIX]Dialog::sm_eventTable;
}

wxEventHashTable [CLASS_PREFIX]Dialog::sm_eventHashTable( [CLASS_PREFIX]Dialog::sm_eventTable );

wxEventHashTable &[CLASS_PREFIX]Dialog::GetEventHashTable() const {
	return [CLASS_PREFIX]Dialog::sm_eventHashTable;
}

const wxEventTableEntry [CLASS_PREFIX]Dialog::sm_eventTableEntries[] = {  // 事件列表
	//(*EventTable( [CLASS_PREFIX]Dialog )
	//*)
	wxEventTableEntry( wxEVT_NULL, 0, 0, 0, 0 )
};

[CLASS_PREFIX]Dialog::[CLASS_PREFIX]Dialog( wxWindow* parent, wxWindowID id ) {  // 创建图形界面
	//(*Initialize( [CLASS_PREFIX]Dialog )
	//*)
}

[CLASS_PREFIX]Dialog::~[CLASS_PREFIX]Dialog() {
	//(*Destroy( [CLASS_PREFIX]Dialog )
	//*)
}

void [CLASS_PREFIX]Dialog::OnQuit( wxCommandEvent& event ) {  // 退出程序触发此函数
	Close();
}

void [CLASS_PREFIX]Dialog::OnAbout( wxCommandEvent& event ) {  // 单击按钮触发此函数
	wxString msg(L"哈喽 欢迎使用图形库");
	sms( msg, L"欢迎..." );
}
[ENDIF WXDIALOG]
