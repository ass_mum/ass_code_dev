// ***************************************************************
//   时间: [NOW]
//   用户: [AUTHOR_NAME] ([AUTHOR_WWW])
// ***************************************************************

[PCH_INCLUDE]#include "[FILENAME_PREFIX]App.h"

//(*AppHeaders
//*)

wxAppConsole* wxCreateApp() {
	return new [CLASS_PREFIX]App;
}

wxAppInitializer wxTheAppInitializer( ( wxAppInitializerFunction ) wxCreateApp );

extern [CLASS_PREFIX]App& wxGetApp();

[CLASS_PREFIX]App& wxGetApp() {
	return* wx_static_cast( [CLASS_PREFIX]App*, wxApp::GetInstance() );
}

int main( int argc, char** argv, char* envp[] ) {
	return wxEntry( argc, argv );
}

bool [CLASS_PREFIX]App::OnInit() {
	//(*AppInitialize
	//*)
	return wxsOK;
}
