m_WizType <- wizProject;
m_WantPCH <- false; // 使用预编译头文件
m_IsEmpty <- false; // 创建空的项目
m_IsPartialDebug <- false; // 调试库链接
m_LibDebugSuffix <- _T( "d" ); // 调试图形库
m_ProjAuthor <- _T( "m" ); // 作者姓名
m_ProjWebsite <- _T( "https://ycyskp.coding-pages.com/" ); // 作者网站
m_PCHFileName <- _T( "wx_pch.h" ); // PCH文件名
m_GuiAppType <- 0; // 应用程序类型: 0 - 常规 1 - 模态

function BeginWizard() {
	Wizard.AddProjectPathPage();
	Wizard.AddPage( _T( "WxConf" ) );
	Wizard.AddCompilerPage( _T( "" ), _T( "*" ), true, true );
}

function OnEnter_WxConf( fwd ) {
	if( fwd ) {
		local Author, Website;
		Author = GetConfigManager().Read( _T( "/wx_project_wizard/author" ), _T( "" ) );
		Website = GetConfigManager().Read( _T( "/wx_project_wizard/website" ), _T( "" ) );
		if( IsNull( Author ) || IsNull( Website ) ) {
			m_ProjAuthor = Author;
			m_ProjWebsite = Website;
		}
		local AppType = ConfigManager.Read( _T( "/wx_project_wizard/guiapptype" ), -1 );
		if( AppType != -1 ) {
			m_GuiAppType = AppType;
		}
		local WantPCH = ConfigManager.Read( _T( "/wx_project_wizard/pch" ), -1 );
		if( WantPCH != -1 ) {
			m_WantPCH = WantPCH;
		}
		Wizard.SetCheckCheckbox( _T( "chkWxConfPCH" ), IntToBool( m_WantPCH ) );
		Wizard.SetTextControlValue( _T( "txtProjAuthor" ), m_ProjAuthor );
		Wizard.SetTextControlValue( _T( "txtProjWebsite" ), m_ProjWebsite );
		Wizard.SetRadioboxSelection( _T( "RB_GUIAppType" ), m_GuiAppType );
	}
	return true;
}

function OnLeave_WxConf( fwd ) {
	if( fwd ) {
		if( !( "WxsAddWxExtensions" in getroottable() ) ) {
			ShowInfo( _T( "wxSmith插件未加载，无法继续" ) );
			return false;
		}
		m_GuiAppType = Wizard.GetRadioboxSelection( _T( "RB_GUIAppType" ) );
		GetConfigManager().Write( _T( "/wx_project_wizard/guiapptype" ), m_GuiAppType );
		m_ProjAuthor = Wizard.GetTextControlValue( _T( "txtProjAuthor" ) );
		GetConfigManager().Write( _T( "/wx_project_wizard/author" ), m_ProjAuthor );
		m_ProjWebsite = Wizard.GetTextControlValue( _T( "txtProjWebsite" ) );
		GetConfigManager().Write( _T( "/wx_project_wizard/website" ), m_ProjWebsite );
		m_WantPCH = Wizard.IsCheckboxChecked( _T( "chkWxConfPCH" ) );
		ConfigManager.Write( _T( "/wx_project_wizard/pch" ), BoolToInt( m_WantPCH ) );
		m_WantPCH = Wizard.IsCheckboxChecked( _T( "chkWxUnixConfPCH" ) );
		if( !GetCompilerFactory().CompilerInheritsFrom( Wizard.GetCompilerID(), _T( "gcc*" ) ) && m_WantPCH ) {
			ShowWarning( _T( "预编译头文件目前只适用于GNU GCC.\n 它们对于所有其他编译器都是禁用的." ) );
			m_WantPCH = false;
		}
		m_IsEmpty = Wizard.IsCheckboxChecked( _T( "chkWxUnixEmpty" ) );
		if( m_IsEmpty && m_WantPCH ) {
			local msg = _T( "您已经为空项目选择了PCH支持.\n" );
			msg = msg + _T( "向导将不添加PCH支持，因为它不能在不添加任何文件的情况下添加.\n\n" );
			msg = msg + _T( "请稍后将PCH标头添加到项目中" );
			ShowInfo( msg );
			m_WantPCH = false;
		}
		m_PCHFileName = _T( "wx_pch.h" );
	}
	return true;
}

function GetGeneratedFile( file_index ) {
	if( !m_IsEmpty ) {
		local Prefix = GetFixedProjectName( Wizard.GetProjectName() );
		if( file_index == 0 ) {
			return Prefix + _T( "App.h" ) + _T( ";" ) + GenerateHeader( file_index );
		} else if( file_index == 1 ) {
			return Prefix + _T( "App.cpp" ) + _T( ";" ) + GenerateSource( file_index );
		} else if( file_index == 2 ) {
			return Prefix + _T( "Main.h" ) + _T( ";" ) + GenerateHeader( file_index );
		} else if( file_index == 3 ) {
			return Prefix + _T( "Main.cpp" ) + _T( ";" ) + GenerateSource( file_index );
		}
		if( file_index == 4 ) {
			if( m_GuiAppType == 0 ) {
				return _T( "wxsmith/" ) + Prefix + _T( "dialog.mui" ) + _T( ";" ) + GenerateSource( file_index );
			} else {
				return _T( "wxsmith/" ) + Prefix + _T( "frame.mui" ) + _T( ";" ) + GenerateSource( file_index );
			}
		}
		if( file_index == 5 && m_WantPCH ) {
			return _T( "wx_pch.h" ) + _T( ";" ) + GenerateHeader( file_index );
		}
	}
	return _T( "" );
}

function SetupProject( project ) {
	project.AddLinkerOption( _T( "-lwx_base" ) );
	project.AddLinkerOption( _T( "-lwx_base_xml" ) );
	project.AddLinkerOption( _T( "-lglib-2.0" ) );
	project.AddLinkerOption( _T( "-lgtk-x11-2.0" ) );
	project.AddLinkerOption( _T( "-lgdk-x11-2.0" ) );
	project.AddLinkerOption( _T( "-lpangocairo-1.0" ) );
	project.AddLinkerOption( _T( "-latk-1.0 " ) );
	project.AddLinkerOption( _T( "-lcairo" ) );
	project.AddLinkerOption( _T( "-lgdk_pixbuf-2.0" ) );
	project.AddLinkerOption( _T( "-lgio-2.0" ) );
	project.AddLinkerOption( _T( "-lpangoft2-1.0" ) );
	project.AddLinkerOption( _T( "-lpango-1.0" ) );
	project.AddLinkerOption( _T( "-lgobject-2.0" ) );
	project.AddLinkerOption( _T( "-lfontconfig" ) );  // 字体配置
	project.AddLinkerOption( _T( "-lfreetype" ) );  // 字体引擎
	project.AddIncludeDir( _T( "/usr/include/" ) );
	project.AddIncludeDir( _T( "/home/m/Source/ass_code/include" ) );
	project.AddLibDir( _T( "/lib" ) );
	local target = project.GetBuildTarget( Wizard.GetDebugName() );
	if( !IsNull( target ) ) {
		SetupTarget( target, true );
	}
	local target = project.GetBuildTarget( Wizard.GetReleaseName() );
	if( !IsNull( target ) ) {
		SetupTarget( target, false );
	}
	if( m_WantPCH && GetCompilerFactory().CompilerInheritsFrom( Wizard.GetCompilerID(), _T( "gcc*" ) ) ) {
		local pchfile = project.GetFileByFilename( m_PCHFileName, true, true );
		if( !IsNull( pchfile ) ) {
			pchfile.compile = true;
			pchfile.link = false;
			pchfile.weight = 0;
			project.SetModeForPCH( pchSourceDir );
			project.AddCompilerOption( _T( "-Winvalid-pch" ) );
			project.AddCompilerOption( _T( "-include " ) + m_PCHFileName );
			project.AddCompilerOption( _T( "-DWX_PRECOMP" ) );
		}
	}
	WarningsOn( project, Wizard.GetCompilerID() );
	local target = project.GetBuildTarget( Wizard.GetDebugName() );
	if( !IsNull( target ) ) {
		SetupTarget( target, true );
	}
	target = project.GetBuildTarget( Wizard.GetReleaseName() );
	if( !IsNull( target ) ) {
		SetupTarget( target, false );
	}
	if( "WxsAddWxExtensions" in getroottable() ) {
		local Prefix = GetFixedProjectName( Wizard.GetProjectName() );
		local WxsFileName = _T( "" );
		if( m_GuiAppType == 0 ) {
			WxsFileName = _T( "dialog.mui" );
		} else {
			WxsFileName = _T( "frame.mui" );
		}
		WxsAddWxExtensions( project, Prefix + _T( "App.cpp" ), Prefix + _T( "Main.cpp" ), Prefix + _T( "Main.h" ),
												_T( "wxsmith/" ) + Prefix + WxsFileName );
	}
	return true;
}

function SetupTarget( target, is_debug ) {
	if( IsNull( target ) ) {
		return false;
	}
	local obj_output_dir, exe_file_name, exe_output_dir;
	if( m_WizType == wizProject ) {
		if( is_debug ) {
			obj_output_dir = Wizard.GetDebugObjectOutputDir();
			exe_output_dir = Wizard.GetDebugOutputDir();
		} else {
			obj_output_dir = Wizard.GetReleaseObjectOutputDir();
			exe_output_dir = Wizard.GetReleaseOutputDir();
		}
		exe_file_name = Wizard.GetProjectName();
	} else if( m_WizType == wizTarget ) {
		obj_output_dir = Wizard.GetTargetObjectOutputDir();
		exe_output_dir = Wizard.GetTargetOutputDir();
		exe_file_name = target.GetParentProject().GetTitle();
	}
	target.SetTargetType( ttExecutable );  // 启用图形类型
	target.SetOutputFilename( exe_output_dir + exe_file_name + DOT_EXT_EXECUTABLE );
	if( is_debug ) {
		DebugSymbolsOn( target, Wizard.GetCompilerID() );
	} else {
		OptimizationsOn( target, Wizard.GetCompilerID() );
	}
	target.SetOptionRelation( ortLinkerOptions, orPrependToParentOptions );
	if( !is_debug || m_IsPartialDebug ) {
		m_LibDebugSuffix = _T( "" );
	}
	return true;
}

function GetTemplateFile( index ) {
	local template_file = _T( "" );
	if( index == 0 ) {
		template_file = _T( "wxwidgets/wxsmith/app.h" );
	} else if( index == 1 ) {
		template_file = _T( "wxwidgets/wxsmith/app.cpp" );
	} else if( index == 2 ) {
		template_file = _T( "wxwidgets/wxsmith/main.h" );
	} else if( index == 3 ) {
		template_file = _T( "wxwidgets/wxsmith/main.cpp" );
	} else if( index == 4 ) {
		template_file = _T( "wxwidgets/wxsmith/resource.mui" );
	}
	return template_file;
}

function GenerateHeader( index ) {
	local path = Wizard.FindTemplateFile( GetTemplateFile( index ) );
	local buffer = IO.ReadFileContents( path );
	return SubstituteMacros( buffer );
}

function GenerateSource( index ) {
	local path = Wizard.FindTemplateFile( GetTemplateFile( index ) );
	local buffer = IO.ReadFileContents( path );
	return SubstituteMacros( buffer );
}

function SubstituteMacros( buffer ) {
	if( m_GuiAppType == 0 ) {
		buffer  = HandleDirective( buffer, _T( "WXDIALOG" ), true );
		buffer  = HandleDirective( buffer, _T( "WXFRAME" ), false );
	} else if( m_GuiAppType == 1 ) {
		buffer  = HandleDirective( buffer, _T( "WXDIALOG" ), false );
		buffer  = HandleDirective( buffer, _T( "WXFRAME" ), true );
	}
	buffer = HandleDirective( buffer, _T( "WINDOWS" ), false );
	local Wizard = Wizard.GetProjectName();
	local Prefix = GetFixedProjectName( Wizard );
	local PchInclude = m_WantPCH ? ( _T( "#include \"" ) + m_PCHFileName + _T( "\"\n" ) ) : _T( "" );
	buffer.Replace( _T( "[PROJECT_HDR]" ), Prefix.Upper() );
	buffer.Replace( _T( "[FILENAME_PREFIX]" ), Prefix );
	buffer.Replace( _T( "[CLASS_PREFIX]" ), Prefix );
	buffer.Replace( _T( "[AUTHOR_NAME]" ), m_ProjAuthor );
	buffer.Replace( _T( "[AUTHOR_WWW]" ), m_ProjWebsite );
	buffer.Replace( _T( "[NOW]" ), ReplaceMacros( _T( "$(TODAY)" ), false ) );
	buffer.Replace( _T( "[PCH_INCLUDE]" ), PchInclude );
	return buffer;
}

function HandleDirective( buffer, directive, enabled ) {
	local dir_if = _T( "[IF " ) + directive + _T( "]" );
	local dir_endif = _T( "[ENDIF " ) + directive + _T( "]" );
	while( true ) {
		local findStart = buffer.Find( dir_if );
		if( findStart == -1 ) {
			return buffer;
		}
		local findEnd = buffer.Find( dir_endif );
		if( findEnd == -1 || findEnd <= findStart ) {
			return buffer;
		}
		local block = buffer.Mid( findStart, findEnd - findStart );
		local findElse = block.Find( _T( "[ELSE]" ) );
		if( !enabled ) {
			if( findElse == -1 ) {
				buffer.Remove( findStart, ( findEnd - findStart ) + dir_endif.Length() );
			} else {
				buffer.Remove( findEnd, dir_endif.Length() );
				buffer.Remove( findStart, findElse + 6 );
			}
		} else {
			if( findElse == -1 ) {
				buffer.Remove( findEnd, dir_endif.Length() );
				buffer.Remove( findStart, dir_if.Length() );
			} else {
				local start = findStart + findElse;
				buffer.Remove( start, ( findEnd - start ) + dir_endif.Length() );
				buffer.Remove( findStart, dir_if.Length() );
			}
		}
	}
	return buffer;
}

function IntToBool( val ) {
	return ( val == 0 ? false : true );
}

function BoolToInt( val ) {
	return ( val ? 1 : 0 );
}
